<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model {

    protected $fillable = [
        'title',
        'seo_title',
        'lead',
        'content',
        'status',
        'publish_on',
        'hl',
        'user_id',
        'author'
    ];

}
