<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Mail;
use Session;
use Validator;
use App\Message;
class pageController extends Controller
{
    public function about(){
        return 'about';
    }
    
     public function contact(){
         $userdata = [
             'firstname' => 'György',
             'lastname' => 'Horváth',
         ];
        
        $date = Carbon::now('Europe/Budapest')->toDayDateTimeString();
        return view('contact',compact('userdata','date'));
    }
    public function sendMessage(Request $request){
        
        $rules = [
            'email' => 'required|email',
            'subject' => 'required',
            'message' => 'required|min:5'
        ];
        $messages = [
            'email' => 'Email valós kell legyen!',
            'email.required' => 'Email köll!',        
            'subject.required' => 'Tárgy kötelező!',
            'message.required' => 'Üzenet kötelező!',
            'message.min' => 'Üzenet min :min karakter kell legyen!'
        ];
        
        $validator = Validator::make($request->all(),$rules,$messages);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        //dd($request);
        $user_email = $request->input('email');
        $msg = $request->input("message");
        $subject = $request->input("subject");
        //tároljuk el a messages táblában az üzenetet
        $message = new Message;
        $message->email = $user_email;
        $message->subject = $subject;
        $message->message = $msg;
        $message->save();
        //dump($request->input("message"),$request->input("subject"));
         Mail::send('emails.contactemail', compact('msg','user_email','subject'), function ($m) use ($user_email) {
            $m->from($user_email, 'Your Application');
            $m->to('info@myapp.com')->subject('Message from contact form!');
        });
        //sikeres üzenetküldés kiírása
        Session::flash('status','Email sent successfully!');
        return redirect()->back();
    }
}
