<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use Faker;//dummy content generator

class ArticlesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles= Article::all();
        return view('articles.index',compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //itt a form kéne legyen, de most csinálunk egy dummy cikket
        $faker = Faker\Factory::create();
        $title=$faker->sentence(6);
        $data=[
          'title'=> $title,  
          'seo_title'=> str_slug($title),
          'lead'=>$faker->sentences(3,true),
          'content'=>$faker->paragraphs(3,true),
          'publish_on'=>$faker->dateTime(),
          'user_id'=>1,
          'author'=>$faker->name(),
          'status'=>1,
          'hl'=>0
        ];
        //dd($data);
        $article = new Article($data);
        $article->save();
        return view('articles.create',compact('article'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }
}
