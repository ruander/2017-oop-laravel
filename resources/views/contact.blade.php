@extends('layouts.master')

@section('content-title','Kapcsolat')

@section('content')


Név: {{ $userdata['lastname'] }} <b>{{ $userdata['firstname'] }}</b>
<br>
<time datetime="{{$date}}">{{$date}}</time>


<div class="col-xs-12">
    
    @if($errors->any())
    @include('partials.errors')
    @endif
    
    @if(Session::has('status'))
    @include('partials.status',['type'=>'success','msg'=>Session::pull('status')])
    @endif
    
    {!! Form::open(['url' => 'contact']) !!}
    <div class="form-group">
        {!! Form::label('email', 'E-Mail Address', ['class' => 'awesome']) !!}
        {!! Form::email('email',null,['placeholder'=>'email address']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('subject', 'Tárgy') !!}
        {!! Form::text('subject',null,['placeholder'=>'email subject']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('message', 'Üzenet') !!}
        {!! Form::textarea('message',null,['placeholder'=>'email message']) !!}
    </div>
    {!! Form::submit("mehet") !!}
    {!! Form::close() !!}   
</div>
@endsection