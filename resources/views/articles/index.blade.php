@extends('layouts.master')

@section('content-title','All articles')

@section('content')
<ul>
    @forelse ($articles as $article)
    <li>{{ $article->title }}</li>
    @empty
    <li>No articles</li>
    @endforelse
</ul>
@endsection
