@extends('layouts.master')

@section('content-title','Article created!')

@section('content')
<h1>{{$article->title}} ({{$article->id}})</h1>
<a href="{{route('articles')}}">back to articles list</a>
@endsection
