<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ruander | Haladó PHP | Laravel alapok</title>
        <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/bootstrap-theme.min.css')}}" rel="stylesheet">
        <!-- Styles -->
        <link type="text/css" href="css/custom.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            @include('partials.navigation')
            <article class="container main">  
                <div class="row">
                    <header class="col-xs-12">
                        @yield('content-title')
                    </header>
                </div>
                <div class="row">
                    <section class="col-xs-12">
                        @yield('content')
                    </section>
                </div>
            </article>
        </div>   
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{asset('js/bootstrap.min.js')}}"></script>
<!--        <script src="js/docs.min.js"></script>-->
        <script src="js/ie10-viewport-bug-workaround.js" type="text/javascript"></script>
    </body>
</html>
