<!-- Fixed navbar -->
<nav class="navbar navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Bootstrap theme</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="#">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Articles <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('articles') }}">All articles</a></li>
                        <li><a href="{{ route('article-create') }}">New article</a></li>
                       
                    </ul>
                </li>
                @if (Route::has('login'))
               
                    @if (Auth::check())
                    <li class="dropdown top-right links">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">user neve <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/">profile</a></li>
                            <li><a href="{{ route('logout') }}">logout</a></li>
                        </ul>
                    </li>
                    @else
                    <a href="{{ url('/login') }}">Login</a> |
                    <a href="{{ url('/register') }}">Register</a>
                    @endif
               
                @endif 

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>