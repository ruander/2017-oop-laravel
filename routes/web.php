<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Route::get('home', function () {
    return 'home';
});

Route::get('about','pageController@about');
Route::get('contact','pageController@contact');
Route::post('contact','pageController@sendMessage');
Auth::routes();

Route::get('/home', 'HomeController@index');
//articles routok
Route::get('articles/create','ArticlesController@create')->name('article-create');
Route::get('articles','ArticlesController@index')->name('articles');
